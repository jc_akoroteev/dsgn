Change Log
==========================


Develop
----------

v0.0.0.19
----------
* Task #19526656: DSGN Additional @ak
* Task #19869042: Add css frid layout to the project
* Bug #5: Popup is missed @ak
* Bug #7: Do not use div as a decorator and use button tag for button @ak
* Bug #8: Google maps placeholder required @ak
* Bug #10: There is no need to use underscore in the filenames because our starter kit compiles only style.scss @ak
* Bug #11: These styles are already defined in the reset file @ak
* Bug #14: You missed icon codes @ak
* Bug #18: Coordinates and image should not be hardcoded in the JS file. The should be editable from backend @ak
* Bug #19: Class and delay are also variable @ak
* Bug #21: The line is missed @ak
* Bug #22: The line color does not match the design @ak
* Bug #23: Bg font does not match the design and check text align in the block with heart @ak
* Bug #24: Font in list shorthand
* Bug #25: Imports in main file
* Bug #26: Delete @media screen and (-ms-high-contrast: active), screen and (-ms-high-contrast: active)
* Bug #27: Delete lick from package.json
* Bug #28: Marker inside index,html
* Bug #29: Delete equal attributes