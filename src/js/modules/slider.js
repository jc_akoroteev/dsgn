const slider = () => {

    const thumbsSliderParams = {

        slidesPerView: 2,
        watchSlidesProgress: true,
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        on: {
            slideNextTransitionStart: function () {
                mainSlider.slideNext();
            },
            slidePrevTransitionStart: function () {
                mainSlider.slidePrev();
            }
        },
    };

    let galleryThumbs = new Swiper('.slider-thumbs', thumbsSliderParams);

    enquire.register("screen and (max-width: 768px)", {

        match: function () {
            galleryThumbs.destroy(false, false);
            galleryThumbs = null;
        },
        unmatch: function () {
            galleryThumbs = new Swiper('.slider-thumbs', thumbsSliderParams);
        },

    });

    let mainSlider = new Swiper('.slider', {

        slidesPerView: 1,
        centeredSlides: true,
        thumbs: {
            swiper: galleryThumbs
        }
    });

    return galleryThumbs, mainSlider;
};

export default slider;