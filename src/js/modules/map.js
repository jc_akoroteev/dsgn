const gMap = () => {

  let map;
  let marker;
  let styles;

  if (typeof map === null || typeof map === undefined) {
    return;

  } else {
    map = new google.maps.Map(
      document.getElementById('map'), {
        zoom: 11,
        center: uluru,
        disableDefaultUI: true
      });
    marker = new google.maps.Marker({
      position: uluru,
      map: map,
      icon: image
    });
    styles = [{
        featureType: 'landscape',
        elementType: 'geometry',
        stylers: [{
          color: '#dbdcd5'
        }]
      }, {
        featureType: "landscape.natural",
        elementType: "all",
        stylers: [{
          color: "#dbdcd5"
        }]
      }, {
        featureType: "road",
        elementType: "geometry",
        stylers: [{
          lightness: 100
        }, {
          visibility: "simplified"
        }]
      }, {
        featureType: 'water',
        elementType: 'geometry.fill',
        stylers: [{
          color: "#c1c2bc"
        }, {
          saturation: 0
        }, {
          lightness: 0
        }]
      }, {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#dbdcd5"
        }, {
          "visibility": "off"
        }]
      }, {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [{
          "color": "#dbdcd5"
        }, {
          "visibility": "off"
        }]
      }, {
        "featureType": "transit",
        "elementType": "labels.text.stroke",
        "stylers": [{
          "visibility": "on"
        }, {
          "color": "#b2b3ad"
        }]
      }, {
        "elementType": "labels.icon",
        "stylers": [{
          "saturation": -100
        }]
      }, {
        "featureType": "transit",
        "elementType": "labels.text.fill",
        "stylers": [{
          "visibility": "on"
        }, {
          "color": "#808080"
        }]
      },
      {
        featureType: 'road.arterial',
        elementType: 'geometry',
        stylers: [{
          visibility: "off"
        }]
      },
      {
        featureType: 'road.local',
        elementType: 'all',
        stylers: [{
          visibility: "off"
        }]
      }
    ];
    map.setOptions({
      styles: styles
    });
  }

};
export default gMap;