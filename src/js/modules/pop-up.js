import slider from "./slider";

const popUp = () => {
	let swiperSlider;
	const $modalLink = $('a.modal');

	$modalLink.fancybox({
		touch: false,
		afterLoad: () => {
			swiperSlider = slider();
		},
		beforeClose: () => {
			swiperSlider.destroy();
			swiperSlider = null;
		}
	});
};

export default popUp;