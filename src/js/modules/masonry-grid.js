const masonryGrid = () => {
  const $grid = $('.grid');
  const masonryOptions = {
    columnWidth: 1,
    percentPosition: true,
    itemSelector: '.grid-item'
  };
  enquire.register("screen and (max-width: 767px)", {
    setup: function () {
      $grid.masonry(masonryOptions);
    },
    match: function () {
      $grid.masonry('destroy');
    },
    unmatch: function () {
      $grid.masonry(masonryOptions);
    }
  });
};

export default masonryGrid;