const menuBtn = () => {

    const $menuBtn = $('.menu-btn');
    const $navMenu = $('.nav-menu');
    const SHOW_MENU = 'show';
    const BUTTON_CLOSE_CLASS = 'close';

    $menuBtn.click(function () {
        $navMenu.toggleClass(SHOW_MENU);
        $menuBtn.toggleClass(BUTTON_CLOSE_CLASS);
        enquire.register("screen and (max-width: 767px)", {
            match: function () {
                scrollLock.toggle();
            },
            unmatch: function () {
                scrollLock.show();
            }
        });
    });

};
export default menuBtn;