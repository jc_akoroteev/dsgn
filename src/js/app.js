import masonryGrid from './modules/masonry-grid';
import menuBtn from './modules/menu-btn';
import gMap from './modules/map';
import popUp from './modules/pop-up';
(($) => {
  'use strict';
  
  $(() => {
    
    menuBtn();
    gMap();
    masonryGrid();
    popUp();
 
  });
  
})(jQuery);