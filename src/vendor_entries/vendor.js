// All imports in this file will be compiled into vendors.js file.
//
// Note: ES6 support for these imports is not supported in base build

module.exports = [
  './node_modules/jquery/dist/jquery.js',
  './node_modules/masonry-layout/dist/masonry.pkgd.min.js',
  './node_modules/enquire.js/dist/enquire.min.js',
  './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
  './node_modules/swiper/dist/js/swiper.min.js',
  './node_modules/scroll-lock/dist/scroll-lock.min.js'
];